.PHONY: all base.target dev.target

MAKEFLAGS=-e PHP_VERSION=7.2.5

all: dev.target

base.target:
	cd base; \
	$(MAKE)

dev.target: base.target
	cd dev; \
	$(MAKE)
